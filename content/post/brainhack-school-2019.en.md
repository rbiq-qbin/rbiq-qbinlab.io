---
title: BrainHack School 2019
author: Alexa Pichet Binette & Samuel Guay
date: 2019-09-26
slug: brainhack-school-2019
categories:
- Education
tags:
- Open Science
- Summer school
description: ''
images:
- "/img/bhs_2019.png"
linktitle: ''

---
Learning how to work openly and collaboratively are becoming increasingly important [not to say necessary] aspects in research. Furthermore, analyzing complex bioimaging data requires knowledge in biology, statistical modeling and computational tools, and adopting good research practices early on is key for success. The Montreal BrainHack School was founded based on these premises by neuro-enthusiasts. Dr. Pierre Bellec, director of the BrainHack School told us: 

*“Brainhack is a reference to the spirit of the hacker community at the origin of free software: playful cleverness and collaboration, but this time for the brain. Brainhack is also the name of a series of 3-days workshops initiated by Cameron Craddock, where multi-disciplinary people gather to collaborate on projects. Over the years, it became clear there was a huge training potential with that format. Brainhack school is an attempt to stretch brainhack into a 4 weeks project-based learning experience.”.*

{{< figure src="https://brainhackmtl.github.io/school2019/assets/images/fig_data_science.png" title="" >}} 

Over four weeks, participants have the chance to learn about the cutting-edge open science tools and to apply them on a project of their choice. Working in collaboration with one another and directly putting into practice new knowledge is the best way to learn and is exactly how the school was designed. Graduate students can earn 3 university credits by attending the school, but in no way does this summer school look like a classic 3-credit course. Each week of the school is organised by a different partner institution in Montreal and is centered around a different theme with a resident expert tutor. This way,  students have the chance to visit the main universities around the city’s namesake mountain and experience different styles of teaching, but always benefit from project-based, hands-on learning. 

A total of 25 students from seven different affiliations participated in the second edition of the BrainHack School from August 5th to 30th, 2019. Most students were from universities located in Montreal, but this year’s edition also attracted students from the University of Toronto and the University of Ontario Institute of Technology in addition to a CEGEP student! 
Designed as a Neuro-Data Science Bootcamp, the first week at McGill University was dedicated to open science topics including open source software, datasets, and machine learning. The computational reproducibility toolkit that students were introduced to enabled them to improve and open their workflow. They could then focus on which tools and techniques they will use for their own projects in following weeks. For newcomers in open science, their general feeling during the first week was best described in one of the attendee’s tweets: {{< tweet 1159163795442475010 >}}  The aim of the first week wasn’t to make them experts on all available tools, but rather to make them aware of their existence, which was accomplished according to the tweet.  

For the second week, students travelled to the other side of the mountain to the Université de Montréal, where they had to define the project they wanted to work and acquire skills on. The range of projects was impressive: from pupillary measurements, electrophysiological recordings in rats, to neuroimaging in diverse clinical populations, a host of bioimaging topics were analyzed over the weeks (see here for all participants’ projects). Collaboration was a central element during Week 2, where everyone had to comment on others’ work. In addition, the organizers made sure all needs were met according to another satisfied participant’s tweet: {{< tweet 1162375407775244288 >}}.

An important component of the training was science communication. At Polytechnique Montréal, Week 3 was devoted to visualization and communication while participants kept working on their respective projects. As scientists, it is not enough to do science openly to make our research accessible. Being able to explain our research projects to a lay audience is also essential to make our work even more accessible. To this end, participants learned about different tools that can be used to create interactive figures and videos. By the end of the week, participants produced a video or infographic to make their research accessible and understandable, and are now well-equipped to continue to do so in the future. 

{{< youtube 8NXdLCt9ltw >}}

Concordia University hosted Week 4, during which participants presented their results and deliverables in small groups and gave feedback to each other. They also planned the written report they had to deliver by September 6th to obtain their credits. The summer school concluded with a BBQ picnic at Beaver Lake on top of Mont Royal. When asked about his favorite highlight of this year’s edition of the BrainHack School, Dr. Bellec couldn’t pick only one, but he did mention how the local and international residents [all listed here] were amazing and really meshed with the students. 

Anne Monnier, a Master student in Neuroscience at the Université de Montréal, had only good things to say about the school. She enrolled in the school to develop a decoding script for her EEG research project and encountered way more than she had expected, she said. The school format and the welcoming community with great values were exactly what she needed to connect with Open Science and start her project! She was also very happy to learn about the supercomputers freely available to researchers through Calcul Québec and Compute Canada.
This second edition was a great success. Dr. Pierre Bellec was very pleased with this year’s improvements:

*“The biggest improvement this year is the addition of a week-long bootcamp at the beginning of the school, lead by JB Poline with the help of Jake Vogel, Elizabeth DuPre and many other volunteers. Students got exposed to a comprehensive stack of neuroimaging and AI python tools, and they were well equipped to choose which tools they want to use in their projects.”*

The feedback from participants and instructors was also very positive. The latter all wished that this course existed when they were starting their graduate training, and the former, in just four weeks, now have a well-rounded overview of open science tools, resources, and community to pursue their research. Innovative initiatives like the BrainHack School contribute to making the bioimaging community in Québec so vibrant, and luckily the school will continue in the next years. Regarding the 2020 edition, Dr. Bellec was already excited: 

*“We've been pulling off the organization of the pilots last minute for two years, but this is about to change as we've matured the formula enough. Also, we were supported this year by UNIQUE, a Quebec neuro-AI network funded by FQRNT, as well as the Canadian Open Neuroscience Platform (CONP). Expect the AI component of BrainHack School to strengthen next year, and also have more training on neuro in order to attract more computer science and engineering students. We're hoping that next year McGill and Polytechnique will credit the school too, and that we will find new partners.”*

Thanks to [UNF](https://unf-montreal.ca/home/), [CRIUGM](http://www.criugm.qc.ca/fr.html), the [Canadian Open Neuroscience Platform](https://conp.ca/) and UNIQUE for making this initiative possible. 

Useful links:

* [BrainHack School Website](https://brainhackmtl.github.io/school2019/)
* [List of participants’ open projects](https://github.com/mtl-brainhack-school-2019/)
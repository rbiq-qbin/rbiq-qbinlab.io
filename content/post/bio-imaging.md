---
title: What is Bio-imaging?
author: The communications team
date: 2019-12-05
slug: what-is-bio-imaging
categories:
- Education
tags:
- SciComm
- Bio-imaging
description: ''
images: 
linktitle: ''

---
# What is Bio-imaging?

Bio-imaging is a term used to describe any scientific technique that can be used to look at (or inside!) biological tissue and organisms. Below you can learn about some of the many different bio-imaging methods used by researchers across Quebec:

### Positron emission tomography (PET)

{{< figure src="https://rbiq-qbin.gitlab.io/img/bio-imaging/PET.gif" title="" >}}

PET imaging is a functional imaging technique that is used to observe metabolic processes in the body. A PET scan involves ingesting, inhaling, or injecting a small amount of a radioactive tracer, which then distributes itself throughout the body or organ of interest and can is then detected by the scanner. Radiotracers are biologically active molecules that are labeled with a small amount of radioactive material and are designed to bind to specific proteins or sugars in the body or to accumulate in cancerous tumors or areas of inflammation. The scanner then detects the presence of the tracer in different parts of the body and three-dimensional images of tracer concentration are constructed by computer analysis.

PET scanning is heavily used in clinical settings such as for cancer diagnosis and monitoring and it is also an important research tool for mapping normal human heart and brain function.

_Left: Whole-body PET scan: the normal brain and kidneys are labeled and radioactive urine from breakdown of the tracer is seen in the bladder. In addition, a large metastatic tumor mass from colon cancer is seen in the liver. (Image source: Wikimedia Commons)_

### Computed tomography (CT)

CT uses X-ray technology to take a series of images from different angles and then combines them using computer processing to form cross-sectional images of different body tissues and bones. Like X-ray, this allows researchers to see inside the body without cutting or any other invasive procedures, but CT imaging provides more detailed information.

To learn about the physics of x-ray technology, watch this short video by TED-Ed:

{{< youtube gsV7SJDDCY4 >}}

### Magnetic resonance imaging (MRI)

{{< figure src="https://rbiq-qbin.gitlab.io/img/bio-imaging/MRI.jpg" title="" >}}
_An MRI scanner (Image source: Wikimedia Commons)_

Magnetic Resonance Imaging, or MRI, makes use of a large and powerful magnet combined with radio waves and an advanced computer to take detailed images inside parts of the body.

MRI technology relies on the magnetic properties of the water molecules that make up approximately 70% of the human body. When exposed to a strong magnetic field, water molecules line up and spin at a particular frequency. The MRI scanner then sends a radio-frequency pulse, which turns the molecules to face in a different direction. When the pulse is turned off, the molecules begin to realign, and as they do, their frequency changes. A special coil inside the MRI scanner then measures these changes, and since the molecules behave differently in different tissues, this creates contrast in the resulting images and allows detailed 3D images of the inside of body parts to be reconstructed by a computer.

MRI is considered very safe and does not use ionizing radiation, but since it is performed using a very powerful magnet, it cannot be performed on people with metal implants or pacemakers. In addition, it is very loud and requires the person being scanned to lie perfectly still for a prolonged period of time inside a very narrow tube, which can be problematic for certain populations.

MRI can be used to produce many different types of images with various different research applications, some of which are illustrated below (_Images sourced from Wikimedia Commons_):

{{< figure src="https://rbiq-qbin.gitlab.io/img/bio-imaging/structMRI.gif" title="" >}}

**Structural MRI**, as its name suggests, allows researchers and clinicians to take detailed 3D images of the anatomical structure of body parts, like the brain. It has many different applications, for example to identify differences between populations or changes over time in anatomy, or to identify injuries or tumors.

{{< figure src="https://rbiq-qbin.gitlab.io/img/bio-imaging/diffMRI.png" title="" >}}

**Diffusion MRI** is another form of structural MRI that is able to track anatomical connections in the brain. Since water molecules in the brain are constrained by the structure of the tissue they are in, tracking their movement (or diffusion) allows us to determine the orientation of the nerves fibers or axons that make up the _white matter_ of the brain.

{{< figure src="https://rbiq-qbin.gitlab.io/img/bio-imaging/funcMRI.jpg" title="" >}}

**Functional MRI** allows scientists to indirectly measure brain activity over time by detecting changes associated with blood flow. When brain areas become active, blood flow to those regions increases, and more oxygen is consumed. Since oxygenated and de-oxygenated blood have different magnetic properties, the MRI scanner can detect which regions of the brain are active at different points in time during the scan (known as the BOLD effect). This can effect can be used either to investigate which regions of the brain are involved in particular tasks or respond to specific stimuli (task-based fMRI), or to measure spontaneous fluctuations in brain activity during rest (resting-state fMRI).

{{< figure src="https://rbiq-qbin.gitlab.io/img/bio-imaging/cardioMRI.gif" title="" >}}

**Cardiovascular MRI** uses similar uses MRI technology to study the function of the heart and vasculature of the body. It is often used in clinical contexts for the diagnosis of cardiac disease by combining it with electrocardiography, which provides information about the electrical activity of the heart. Recently, new research techniques have also been developed to accurately map the blood vessels of the brain. This area of research is particularly important because since many brain imaging methods rely on or are heavily affected by local changes in blood flow.

### Electroencephalography (EEG)

EEG is a neuroimaging technique used to record electrical signals in the brain through multiple electrodes placed on the scalp. Typically, EEG is used either to measure the brain’s response to a particular stimulus (known as event-related potentials), or to analyse the spectral content of brain activity by looking at various types of brain waves (neural oscillations) at different frequencies.

{{< figure src="https://rbiq-qbin.gitlab.io/img/bio-imaging/EEGsetup.jpg" title="" >}}
_A typical EEG setup (Image source: Wikimedia Commons)_

{{< figure src="https://rbiq-qbin.gitlab.io/img/bio-imaging/EEGdata.png" title="" >}}
_An example of brain waves measured with EEG (Image source: Wikimedia Commons)_

While other brain imaging techniques like MRI and CT have high spatial resolution (produces high-quality images), EEG has excellent temporal resolution in the millisecond range (produces highly detailed information about changes in brain activity over time).

In the clinic, EEG is most often used to diagnose neurological conditions such as epilepsy and sleep disorders. In research, EEG has a large number of potential applications and can be combined with other imaging techniques such as MRI to produce data with high resolution in both spatial and temporal domains.

### Magnetoencephalography (MEG)

{{< figure src="https://rbiq-qbin.gitlab.io/img/bio-imaging/MEG.jpg" title="" >}}
_A person undergoing MEG (Image source: Wikimedia Commons)_

MEG is a functional neuroimaging technique used to measure brain activity by detecting the subtle magnetic field produced by the electrical currents resulting from brain activity using very sensitive magnetometers. MEG can be used to help determine the function of various parts of the brain, for basic research into cognitive and perceptual brain processes, and for neurofeedback.

The data produced using MEG is similar to that from EEG since it originates from the same neurophysiological processes, but there are several important differences. Like EEG, MEG produces data with very high temporal resolution, but limited spatial resolution, but since magnetic fields are less distorted by the skull and scalp than electrical fields, brain activity visible by MEG can be more accurately localized. However, MEG is also relatively more expensive since it requires large and complex equipment to be installed in a magnetically shielded room.

### Ultrasound Imaging

{{< figure src="https://rbiq-qbin.gitlab.io/img/bio-imaging/ultrasound.jpg" title="" >}}
_An ultrasound image of a fetus at 12 weeks of development (Image source: Wikimedia Commons)_

Ultrasound imaging, or sonography, uses very high-frequency sound to visualize internal organs and tissues. When a soundwave hits an object, it bounces back, or echoes. By sending soundwaves into the body and measuring small changes in the pitch and direction of its echo, it is possible to determine the size, shape, and consistency of tissues and organs. This technology is actually very similar to how bats navigate the world using echolocation!

You have probably heard of ultrasound being used on check the health and heartbeat of fetuses in the uterus of pregnant women, but ultrasound can be used to image any type of body tissue, including the brain and the heart!

### Optical imaging

Optical imaging is a technique used by scientists and clinicians to look inside the body using visible light. By taking advantage of the special properties of photons and the ways different tissues absorb and scatter light, it can yield detailed images at macro- and microscopic levels from organs to cells and even molecules.

Optical imaging is considered safer than other imaging techniques like x-rays, CT, and PET, because it uses non-ionizing radiation including visible ultraviolet and infrared light, and can therefore be used for long and repeated procedures over time, for example to monitor the progression of disease or the results of a disease treatment. Unlike other imaging techniques that are limited to just one type of measurement, optical imaging takes advantage of the various colors of light in order to see and measure many different properties of an organ or tissue at the same time. It can also be combined with other imaging techniques such as MRI or x-ray to provide even more detailed information.

{{< figure src="https://rbiq-qbin.gitlab.io/img/bio-imaging/lightspectrum.jpg" title="" >}}
_The spectrum of visible light as compared to other types of radiation (Image source: NIH)_

### Microscopy

{{< figure src="https://rbiq-qbin.gitlab.io/img/bio-imaging/Ebola.jpeg" title="" >}}
_A colorized electron micrograph of Ebola virus particles (in green) from in infected African Green Monkey Kidney cell (Image source: Wikimedia Commons)_

Microscopy is the field of imaging that uses microscopes to see properties of biological tissue that cannot be seen with the naked eye. There are many different types of microscopes, the most common of which are optical or light microscopes and electron microscopes, both of which involve sending beams of light or electrons onto a sample and measuring and collecting the reflection to create an image.

More specifically, optical microscopy involves passing visible light transmitted through or reflected from the sample through one or more lenses to create a magnified view of the sample, which can then be detected directly by the eye or captured digitally by a computer.

Electron microscopes work by the same principles, but use a beam of accelerated electrons instead of light. These types of microscopes are able to reveal the structure of much smaller objects (have higher resolution) because the wavelength of an electron can be up to 100,000 times shorter than that of visible light photons.

The invention of the microscope in the 17th century revolutionized the field of biology and remains an essential tool in bio-imaging.

To learn more about the invention of the microscope and what can be done with some of the most powerful microscopes in the world, check out these short videos by SciShow:

{{< youtube Ue-86MDmjns >}}

{{< youtube rzGwn6C8OmY >}}